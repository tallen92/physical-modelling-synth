//
//  TremoloGui.h
//  sdaAudioMidi
//
//  Created by Tyrone Allen on 25/02/2013.
//
//

#ifndef __sdaAudioMidi__TremoloGui__
#define __sdaAudioMidi__TremoloGui__

#include <iostream>
#include "../JuceLibraryCode/JuceHeader.h"
#include "AudioMidi.h"

/**
GUI class for the Tremolo class 
 */

class TremoloGui :  public Component,
                    public Slider::Listener,
                    public Button::Listener
{
public:
    
    TremoloGui(AudioMidi& audioMidi_);
    
    ~TremoloGui();
    
    //SliderCallbacks===============================================================
    void sliderValueChanged (Slider* slider);
    
    //ButtonCallbacks===============================================================
    void buttonClicked (Button* button);
    
    //Component
    void resized();
	void paint (Graphics &g);
    
private:
    
    AudioMidi& audioMidi;

    Label RateLabel;
    Label DepthLabel;
    
    Slider RateSlider;
    Slider DepthSlider;
    
    TextButton stateButton; //Not working
};


#endif /* defined(__sdaAudioMidi__TremoloGui__) */
