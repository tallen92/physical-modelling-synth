//
//  titeLabel.cpp
//  sdaAudioMidi
//
//  Created by Tyrone Allen on 15/03/2013.
//
//

#include "titeLabel.h"

titeLabel::titeLabel()
{
    addAndMakeVisible(&title);
    
    Font font("Brush Script MT", 50.0, 2);
    Justification justi(12);
    
    title.setFont(font);
    title.setText("Physical\nModelling\nSynth", false);
}


titeLabel::~titeLabel()
{
    
}

void titeLabel::resized()
{
    title.setBounds(0, 0, getWidth(), getHeight());
}

void titeLabel::paint (Graphics &g)
{
    
}