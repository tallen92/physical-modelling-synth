//
//  BodyResonance.h
//  sdaAudioMidi
//
//  Created by Tyrone Allen on 26/01/2013.
//
//

#ifndef __sdaAudioMidi__BodyResonance__
#define __sdaAudioMidi__BodyResonance__

#include <iostream>
#include "../JuceLibraryCode/JuceHeader.h"
#include "SimpleFilter.h"
#include <cmath>

/**
 Class that models the body of a string instrument that resonates specific fequencies via the use of parallel bandpass filters.
 */

class BodyResonance
{
public:
    
    /**
     Constructor
     */
	BodyResonance();
    
    /**
     Destructor
     */
	~BodyResonance();
    
    /**
     Configures filters
     */
    void filterConfig ();
    
    /**
     Changes centre frequencies of bandpass filters
     */
    void setBody (Float32 lowVal, Float32 midVal, Float32 highVal);
    
    /**
     Processes input value through parallel bandpass filters
     */
    Float32 resonantBody (Float32 fOut);
    
    
private:
    
    enum
    {
        kFilter0 = 0,
        kFilter1,
        kFilter2,
        kNumberOfFilters
    };
    SimpleFilter *pFilters[kNumberOfFilters];
    
    CriticalSection sharedMemory;
    
    Atomic<float>increment;
    Atomic<float>low;
    Atomic<float>mid;
    Atomic<float>high;
    
};


#endif /* defined(__sdaAudioMidi__BodyResonance__) */
