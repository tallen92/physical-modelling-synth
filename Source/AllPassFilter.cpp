//
//  AllPassFilter.cpp
//  sdaAudioMidi
//
//  Created by Tyrone Allen on 27/02/2013.
//
//

#include "AllPassFilter.h"

AllPassFilter::AllPassFilter() 
{
    setDelayInSeconds(0.001);
    setDecay(0.1);
}

AllPassFilter::~AllPassFilter()
{
    
}

Float32 AllPassFilter::filter(Float32 input)
{
    Float32 out;
    
    incrPos();
    
    out = delayLineRead();
    
    out = out - input * getDecay();
    
    delayLineWrite(input + out * getDecay()); 
    
    return out;
}