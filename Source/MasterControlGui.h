//
//  MasterControlGui.h
//  sdaAudioMidi
//
//  Created by Tyrone Allen on 10/03/2013.
//
//

#ifndef __sdaAudioMidi__MasterControlGui__
#define __sdaAudioMidi__MasterControlGui__

#include <iostream>
#include "../JuceLibraryCode/JuceHeader.h"
#include "AudioMidi.h"

class MasterControlGui :    public Component,
                            public Slider::Listener,
                            public ActionListener,
                            public Timer

{
public:
    
    MasterControlGui(AudioMidi& audioMidi_);
    
    ~MasterControlGui();
    
    //SliderCallbacks===============================================================
    void sliderValueChanged (Slider* slider);
    
    //ActionListenerCallback===========================================
    void actionListenerCallback (const String& message);
    
    //TimerCallbacks===============================================================
    void timerCallback (void);
    
    //Component
    void resized();
	void paint (Graphics &g);
    
private:
    
    AudioMidi& audioMidi;
    
    Slider gainSlider;
    Slider meterSlider;
    
    Label gainLabel;
    Label meterLabel;
    
    CriticalSection sharedMemory;
    
};


#endif /* defined(__sdaAudioMidi__MasterControlGui__) */
