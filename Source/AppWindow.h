/*
 *  JuceWindow.h
 *  sdaProj
 */

#ifndef H_APPWINDOW
#define H_APPWINDOW

#include "../JuceLibraryCode/JuceHeader.h"

/**
 The application window
 */
class AppWindow  : public DocumentWindow
{
public:
	//==============================================================================
    /**
     Constructor
     */
	AppWindow();
    
    /**
     Destructor
     */
	~AppWindow();
	//==============================================================================
	// called when the close button is pressed or esc is pushed
	void closeButtonPressed();
	
};



#endif //H_APPWINDOW