//
//  SectionLabel.cpp
//  sdaAudioMidi
//
//  Created by Tyrone Allen on 11/03/2013.
//
//

#include "SectionLabel.h"
SectionLabel::SectionLabel()
{
    addAndMakeVisible(&section);
    
    Font font("Brush Script MT", 25.0, 2);
    Justification justi(12);
    
    section.setFont(font);
    
}


SectionLabel::~SectionLabel()
{
    
}

void SectionLabel::resized()
{
    section.setBounds(0, 0, getWidth(), getHeight());
}

void SectionLabel::paint (Graphics &g)
{
    ColourGradient gradient(Colours::darkblue, 0, 0,
                            Colours::darkcyan, getWidth()/2, getWidth()/2,
                            false);
    
    
    g.setColour(Colours::darkblue);
    g.setOpacity(0.1);
    g.drawRoundedRectangle(0, 0, this->getWidth(), this->getHeight(), 8, 10);
    g.fillRoundedRectangle(0, 0, this->getWidth(), this->getHeight(), 8);
    
    
}

void SectionLabel::setLabelText(String newLabelText)
{
    section.setText(newLabelText, false);
}
