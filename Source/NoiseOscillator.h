//
//  NoiseOscillator.h
//  sdaAudioMidi
//
//
//
//

#ifndef __sdaAudioMidi__NoiseOscillator__
#define __sdaAudioMidi__NoiseOscillator__

#include <iostream>
#include "../JuceLibraryCode/JuceHeader.h"
#include "Oscillator.h"

/**
 Generates White noise
 */
class NoiseOscillator  : public Oscillator

{
public:
	//==============================================================================
    /**
     Constructor. When used as an excitation type, noise may not adhere to correct pitches at lower octaves.
     */
	NoiseOscillator(); //create base class, make polymorphic
    
    /**
     Destructor
     */
	~NoiseOscillator();
    /**
     Creates an array of noise samples using a linear congruential generator adapted by Martin Robinson
     */
    float renderWaveShape (const float currentPhase);
    
    /**
     Returns a sample
     */
    float getSample();
private:
	//==============================================================================
    
    
};

#endif /* defined(__sdaAudioMidi__NoiseOscillator__) */
