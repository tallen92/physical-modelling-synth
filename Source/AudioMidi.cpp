//
//  AudioMidi.cpp
//  sdaAudioMidi
//
//  Created by Tyrone Allen on 09/02/2013.
//
//

#include "AudioMidi.h"

AudioMidi::AudioMidi()
{
    audioDeviceManager.initialise(1, 2, 0, true, String::empty, 0);
    audioDeviceManager.addMidiInputCallback(String::empty, this);
    audioDeviceManager.addAudioCallback(this);
    
    for (int i = 0; i <= 3; i++)
    {
        sympString.add(new SympatheticString);
    }
    
    meterValue = 0.f;
    
    pitchBend = 1.0;
    
    exciteOsc = &triOsc;
    
    std::cout<<"Pitch bend in waveguide may be unstable"<<std::endl;
    std::cout<<"Triangle waveform does not like high frequencies"<<std::endl;
    std::cout<<"Noise does not follow pitch in lower octaves"<<std::endl;
    
}

AudioMidi::~AudioMidi()
{
    audioDeviceManager.removeAudioCallback(this);
    audioDeviceManager.removeMidiInputCallback(String::empty, this);
    
    for (int i = 0; i <= 3; i++)
    {
        delete sympString[i];
    }
    
    sympString.clear();
}

//MidiInputCallback=============================================================
void AudioMidi::handleIncomingMidiMessage (MidiInput* source, const MidiMessage& message)
{
    if (message.isController())
    {
        if(message.getControllerNumber() == 1) //Volume of output
        {
            String gainval;
            Float32 newGain = message.getControllerValue()/127.0;
            gainval << "gain:\t"<< newGain << "\n";
            sendActionMessage(gainval);
            
            gain = newGain * newGain * newGain;
        }
    }
    
    else if (message.isNoteOnOrOff())
    {
        
        if (message.isNoteOn())
        {
            exciteOsc.get()->trigger(); // gate to allow excitation generation
            
            if (exciteOsc.get() == &triOsc)
            {
                exciteOsc.get()->pluck(); // triangle wave requires excitation generation per MIDI input
            }
            
        }
        sharedMemory.enter();
        //Set Waveguide resonator delay lines
        pluckString.setTau(MidiMessage::getMidiNoteInHertz(message.getNoteNumber()));
        
        //Tune the sympathetic strings to overtones of fundamental frequency 
        sympString[0]->setTau(MidiMessage::getMidiNoteInHertz(message.getNoteNumber()) * 1.26);
        sympString[1]->setTau(MidiMessage::getMidiNoteInHertz(message.getNoteNumber()) * 1.50);
        
        exciteOsc.get()->setAmplitude(message.getVelocity());
        exciteCtrl.pickVelConfig(message.getVelocity());
        sharedMemory.exit();
    }
    else if (message.isPitchWheel())
    {
       setPitchBend(message.getPitchWheelValue()); //WARNING pitch bend may become unstable
    }
}

//AudioCallbacks================================================================
void AudioMidi::audioDeviceIOCallback (const float** inputChannelData,
                                          int numInputChannels,
                                          float** outputChannelData,
                                          int numOutputChannels,
                                          int numSamples)
{
    const Float32 *inL; //presume only mono input
    Float32 *outL, *outR;
    Float32 fIn = 0.f;
    Float32 fOut = 0.f;
    Float32 ampVal = 0.f;
    Float32 ftemp1, ftemp2, ftemp3;
    
    ftemp1 = ftemp2 = ftemp3 = 0.f;
    
    ampVal = exciteOsc.get()->returnAmplitude();
    
    inL = inputChannelData[0];
    outL = outputChannelData[0];
    outR = outputChannelData[1];
    
    //Filter configuration
    pluckString.filterConfig(1);
    bodyResonance.filterConfig();
    multiFilter.configfilter();
    sympString[0]->filterConfig(3);
    sympString[1]->filterConfig(5);
   // sympString[2]->filterConfig(7);
    
    
    while(numSamples--)
    {
        sharedMemory.enter();
        fIn = exciteOsc.get()->getSample();
        
        fOut = exciteCtrl.exciteProcess(fIn);
        
        fOut = pluckString.FilterProcess(fOut, getPitchBend());
        
        
        ftemp1 = sympString[0]->FilterProcess(fOut) * 0.1;
        ftemp2 = sympString[1]->FilterProcess(fOut) * 0.04;
        
        fOut = fOut + ftemp1 + ftemp2;
        
        fOut = bodyResonance.resonantBody(fOut);
        
        fOut = multiFilter.process(fOut);
        
        fOut *= tremMod.modulation();
        
        calcuateMeterValue(ampVal);
        
        *outL = fOut  * gain.get();
        *outR = fOut  * gain.get();
        sharedMemory.exit();
        
        inL++;
        outL++;
        outR++;
    }
}

void AudioMidi::audioDeviceAboutToStart (AudioIODevice* device)
{
    gain = 0.f;
}

void AudioMidi::audioDeviceStopped()
{
    
}
void AudioMidi::setExcitationType(int ID)
{
    if (ID == 1)
    {
        exciteOsc = &triOsc;
    }
    else if (ID == 2)
    {
        exciteOsc = &noiseOsc;
    }
}

void AudioMidi::setToneControlParam(Float32 pickPosSlider, Float32 StringTensionSlider, Float32 pluckObjectSlider)
{
    exciteCtrl.setPickPos(pickPosSlider);
    pluckString.setRVT(StringTensionSlider);
    exciteOsc.get()->setTRIG_LEN(pluckObjectSlider);
   
}

void AudioMidi::setBodyParam(Float32 lowVal, Float32 midVal, Float32 highVal)
{
    bodyResonance.setBody(lowVal, midVal, highVal);
}


void AudioMidi::setMultiFilterType(SInt16 ID)
{
    multiFilter.setFilterType(ID);
}

void AudioMidi::setMultiFilterParam(Float32 freqVal, Float32 qVal, Float32 gainVal)
{
    if (qVal == 0)//If filter GUI is set to Hp or Lp
    {
        multiFilter.setFilterParam(freqVal);
    }
    else
    {
        multiFilter.setFilterParam(freqVal, qVal, gainVal);
    }
    
}
void AudioMidi::setMultiFilterState(const bool state)
{
    multiFilter.setState(state);
}

bool AudioMidi::getMultiFilterState() const
{
    return multiFilter.getState();
}

void AudioMidi::setTremoloParam(Float32 rateVal, Float32 depthVal)
{
    tremMod.setRateDepth(rateVal, depthVal);
}
void AudioMidi::setTremoloState(const bool state)
{
    tremMod.setState(state);
}

bool AudioMidi::getTremoloState() const
{
    return tremMod.getState();
}

Float32 AudioMidi::getMeterValue() const
{
    return meterValue.get(); 
}

void AudioMidi::setGainValue ( Float32 newGain)
{
    gain = newGain;
}

void AudioMidi::calcuateMeterValue(Float32 ampVal)
{
    if (fabsf(ampVal) > meterValue.get())
    {
        meterValue = fabsf(ampVal);
    }
    
    else
    {
        meterValue = meterValue.get() * 0.9999f;
    }
}

void AudioMidi::setPitchBend(Float32 value)
{
    //shift between a range of ±2 semitones
    pitchBend = value / 128.0;
    
    pitchBend = (pitchBend.get() - 64.0)/32.0;
    
    pitchBend = powf(2.0, pitchBend.get() / 12.0);
}

Float32 AudioMidi::getPitchBend() const
{
    return pitchBend.get();
}

AudioDeviceManager& AudioMidi::getAudioDeviceManager()
{
    return audioDeviceManager;
}
