//
//  BodyResonance.cpp
//  sdaAudioMidi
//
//  Created by Tyrone Allen on 26/01/2013.
//
//

#include "BodyResonance.h"


BodyResonance::BodyResonance()
{
    for(int i = 0; i < kNumberOfFilters; i++)
    {
        pFilters[i] = new SimpleFilter(44100);
    }
    
    increment = 0.0;
    low = 90.0;
    mid = 300.0;
    high = 900;
}


BodyResonance::~BodyResonance()
{
    for(int i = 0; i < kNumberOfFilters; i++)
	{
		delete pFilters[i];
	}
}

void BodyResonance::setBody (Float32 lowVal, Float32 midVal, Float32 highVal)
{
    sharedMemory.enter();
    low = lowVal;
    mid = midVal;
    high = highVal;
    sharedMemory.exit();
}

void BodyResonance::filterConfig ()
{
    pFilters[kFilter0]->FilterConfig(kBPF, low.get(), low.get() / 4.0);
    pFilters[kFilter1]->FilterConfig(kBPF, mid.get(), mid.get() / 7.0);
    pFilters[kFilter2]->FilterConfig(kBPF, high.get(),high.get() / 8.0);
}

Float32 BodyResonance::resonantBody (Float32 fOut)
{
    Float32 output1, output2, output3;
    Float32 amp, amp1, amp2, amp3;
    
    output1 = output2 = output3 = fOut;
    amp = 1.0;
    amp1 = 4.0;
    amp2 = 5.0;
    amp3 = 5.0;
    
    output1 = pFilters[kFilter0]->Filter(output1) * amp1;
    output2 = pFilters[kFilter1]->Filter(output2) * amp2;
    output3 = pFilters[kFilter2]->Filter(output3) * amp3;
    fOut *= amp;
    
    fOut = fOut + output1 + output2 + output3;
    
    return fOut;
}