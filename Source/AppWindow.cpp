/*
 *  JuceWindow.cpp
 *  sdaProj
 */

#include "AppWindow.h"
#include "AppComponent.h"

//==============================================================================
AppWindow::AppWindow()
: 
DocumentWindow(
				"Physical Modelling Synth",                   // Set the text to use for the title
				Colours::teal,					// Set the colour of the window
				DocumentWindow::allButtons,		// Set which buttons are displayed
				true							// This window should be added to the desktop
			  )
{
    setResizable(false, false);
	setTitleBarHeight(20); 
    AppComponent* appComponent = new AppComponent();
    setContentOwned(appComponent, false);
    setMenuBar (appComponent);
}

AppWindow::~AppWindow()
{
 
}

void AppWindow::closeButtonPressed()
{
    JUCEApplication::getInstance()->systemRequestedQuit();
} 