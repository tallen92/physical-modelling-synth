//
//  View.h
//  sdaAudioMidi
//
//  Created by Tyrone Allen on 10/02/2013.
//
//

#ifndef __sdaAudioMidi__View__
#define __sdaAudioMidi__View__

#include <iostream>
#include "ToneControlGui.h"
#include "BodyGui.h"
#include "FilterGui.h"
#include "TremoloGui.h"
#include "MasterControlGui.h"
#include "AudioMidi.h"
#include "SectionLabel.h"
#include "titeLabel.h"
#include "../JuceLibraryCode/JuceHeader.h"

/**
Component that handles all GUI objects used in the application
 */

class View :    public Component
             
{
public:
    
    /**
    Main Gui constructor
     */
    View(AudioMidi& audioMidi_);
    
    /**
     Main Gui deconstructor
     */
    ~View();
    
    //ComponentCallbacks============================================================
	void resized();
	void paint (Graphics &g);
    
    
private:
    
    AudioMidi& audioMidi;
    
    //Sub-Gui
    ToneControlGui toneCtrlGui;
    BodyGui bodyGui;
    FilterGui filterGui;
    TremoloGui tremGui;
    MasterControlGui masterCtrlGui;
    
    SectionLabel toneCtrlLabel;
    SectionLabel bodyLabel;
    SectionLabel filterlabel;
    SectionLabel tremlabel;
    SectionLabel masterCtrlLabel;
    
    titeLabel titleLbl;
    
    
};

#endif /* defined(__sdaAudioMidi__View__) */
