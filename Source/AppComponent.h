/*
 *  AppComponent.h
 *  sdaProj
 */

#ifndef H_APPCOMPONENT
#define H_APPCOMPONENT

#include "../JuceLibraryCode/JuceHeader.h"
#include "AudioMidi.h"
#include "View.h"

/**
 The main component for the applicaiton
 */
class AppComponent  :   public Component,
                        public MenuBarModel

{	
public:
	//==============================================================================
    /**
     Constructor
     */
	AppComponent();
    
    /**
     Destructor
     */
	~AppComponent();
    
	//ComponentCallbacks============================================================
	void resized();
	void paint (Graphics &g); 
    
    //MenuBarEnums/Callbacks========================================================
    enum Menus
	{
		FileMenu=0,
		
		NumMenus
	};
    
    enum FileMenuItems 
	{
        AudioPrefs = 1,
		
		NumFileItems
	};
    StringArray getMenuBarNames();
    PopupMenu getMenuForIndex (int topLevelMenuIndex, const String& menuName);
	void menuItemSelected (int menuItemID, int topLevelMenuIndex); 
private:
	//==============================================================================
    AudioMidi audioMidi; // Audio engine 
    View view; // Graphical USer Interface
    CriticalSection sharedMemory;
    
};

#endif // H_APPCOMPONENT