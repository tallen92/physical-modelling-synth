//
//  Tremolo.cpp
//  sdaAudioMidi
//
//  Created by Tyrone Allen on 19/02/2013.
//
//

#include "Tremolo.h"

Tremolo::Tremolo()
{
    fRate = 19.0;
    fDepth = 0.1;
    sinOsc.setFrequency(fRate.get());
    
    state = true;
}

Tremolo::~Tremolo()
{
    
}

void Tremolo::setRateDepth(Float32 rate, Float32 depth)
{
    fRate = rate;
    fDepth = depth;
    
    sinOsc.setFrequency(fRate.get());
    
}

Float32 Tremolo::modulation()
{
    Float32 fMod = 1.0;
    
    if (state.get() == 1) //Gui button not working 
    {
        fMod = sinOsc.getSample() * fDepth.get();
        
        fMod = fMod * 0.5 + 0.5;
    }
  return fMod;
}

void Tremolo::setState(const bool shouldBeOn)
{
    state = shouldBeOn;
}

bool Tremolo::getState() const
{
    return state.get();
}
