//
//  titeLabel.h
//  sdaAudioMidi
//
//  Created by Tyrone Allen on 15/03/2013.
//
//

#ifndef __sdaAudioMidi__titeLabel__
#define __sdaAudioMidi__titeLabel__

#include <iostream>
#include "../JuceLibraryCode/JuceHeader.h"

class titeLabel : public Component
{
    
public:
    
    titeLabel();
    
    virtual ~titeLabel();
    
    void resized();
    
	void paint (Graphics &g);
    
private:
    
    Label title;
    
};


#endif /* defined(__sdaAudioMidi__titeLabel__) */
