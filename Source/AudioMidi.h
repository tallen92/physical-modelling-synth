//
//  AudioMidi.h
//  sdaAudioMidi
//
//  Created by Tyrone Allen on 09/02/2013.
//
//

#ifndef __sdaAudioMidi__AudioMidi__
#define __sdaAudioMidi__AudioMidi__

#include <iostream>
#include "../JuceLibraryCode/JuceHeader.h"
#include "WaveGuideModel.h"
#include "PluckedString.h"
#include "SympatheticString.h"
#include "Oscillator.h"
#include "NoiseOscillator.h"
#include "TriOscillator.h"
#include "ExcitationControl.h"
#include "BodyResonance.h"
#include "MultiFilter.h"
#include "Tremolo.h"
//way to put all headers in file

/**
 Component that handles all the audio & midi processes of the application.
 */

class AudioMidi   :     public MidiInputCallback,
                        public AudioIODeviceCallback,
                        public ActionBroadcaster
{
public:
    
    /**
     Constructor
     */
    AudioMidi();
    
    /**
     Deconstructor
     */
    ~AudioMidi();
    
    
    //AudioCallbacks================================================================
    void audioDeviceIOCallback (const float** inputChannelData,
                                int numInputChannels,
                                float** outputChannelData,
                                int numOutputChannels,
                                int numSamples);
    
    void audioDeviceAboutToStart (AudioIODevice* device);
    void audioDeviceStopped();
    
    //MidiInputCallback=============================================================
    void handleIncomingMidiMessage (MidiInput* source, const MidiMessage& message);
    
    //=============================================================
    

    
    /**
     Sets the the type of excitation used by the physical model based on the value from the Gui combo box.
     @param ID; 1 = Triangle wave, 2 = noise burst
     */
    void setExcitationType(int ID);
    
    /**
     Sets variables who's values determine the tonal characteristics of the physical model. This is based on values from Tone control GUI sub section
     @param PickPosSlider            The position along the theoretical guitar that the string is picked.
     Middle value = middle of soundhole
     Largest value = near bridge
     
     @param StringDampingSlider     Amount that the plucked string is damped by.
     Low damping = around 0.5s damping time
     High damping = around 3.0s damping time
     
     @param PluckObjectSlider       Value relates to the type of object used to excite guitar
     Low value = pick
     High value = finger
     
     */
    void setToneControlParam(Float32 pickPosSlider,
                             Float32 StringTensionSlider,
                             Float32 pluckObjectSlider);
    /**
     Sets values used in a filter configuration to alter the resonant frequencies of the body of the guitar.
     */
    void setBodyParam(Float32 lowVal, Float32 midVal, Float32 highVal);
    
    /**
     Sets type of filter to be used in the multifilter class.
     
     @param                          ID: 1 = Low Pass 2 = High pass 3 = Bandpass
     */
    void setMultiFilterType(SInt16 ID);
    
    /**
     Sets values used in the configuration of the multifilter.
     */
    void setMultiFilterParam(Float32 freqVal, Float32 qVal=0, Float32 gainVal=0);
    
    /*
     Sets the active state of the multifilter.
     */
    void setMultiFilterState(const bool state);
    
    /*
     Returns the active state of the multifilter.
     */
    bool getMultiFilterState() const;
    
    /**
     Sets the parameters of the tremolo. 
     */
    void setTremoloParam(Float32 rateVal, Float32 depthVal);
    /*
     Sets the active state of the tremolo.
     */
    void setTremoloState(const bool state);
    /*
     Returns the active state of the tremolo.
     */
    bool getTremoloState() const;
    /*
     Sets the value of the pitch bend variable using the pitchbend value of the incoming midi message.
     */
    void setPitchBend(Float32 value);
    /*
     Returns the value of the pitchbend variable to be used in the waveguide model.
     */
    Float32 getPitchBend() const;
    /*
     Returns the AudioDeviceManager to be used in the AppComponent menu callback function.
     */
    AudioDeviceManager& getAudioDeviceManager() ;
    
    /**
     Returns the metervalue
     */
    Float32 getMeterValue()const;
    
    /**
     Sets the gain value
     */
    void setGainValue (Float32 newGain);
    
    /**
     Calculate the meter value
     */
    void calcuateMeterValue(Float32 ampVal);
    
    
private:
    
    AudioDeviceManager audioDeviceManager;
    
    TriOscillator triOsc;
    
    NoiseOscillator noiseOsc;
    
    Atomic<Oscillator*> exciteOsc;
    
    PluckedString pluckString;
    
    Array<SympatheticString*>sympString;
    
    ExcitationControl exciteCtrl;
    
    BodyResonance bodyResonance;
    
    MultiFilter multiFilter;
    
    Tremolo tremMod;
    
    CriticalSection sharedMemory;
    
    //Primitives - atomic
    Atomic<Float32>gain;
    Atomic<Float32>meterValue;
    Atomic<Float32> pitchBend;
};


#endif /* defined(__sdaAudioMidi__AudioMidi__) */
