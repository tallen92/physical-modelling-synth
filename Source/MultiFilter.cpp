//
//  MultiFilter.cpp
//  sdaAudioMidi
//
//  Created by Tyrone Allen on 27/02/2013.
//
//

#include "MultiFilter.h"

MultiFilter::MultiFilter()
{
    for(int i = 0; i < kNumberOfFilters; i++)
    {
        pFilters[i] = new SimpleFilter(44100);
    }
    
    type = kLPF;
    freq = 100.0;
    Q = 0.f;
    gain = 0.f;
    activeState = true;
    
}

MultiFilter::~MultiFilter()
{
    for(int i = 0; i < kNumberOfFilters; i++)
    {
        delete pFilters[i];
    }
}

void MultiFilter::configfilter()
{
    if (type.get() == kLPF)
    {
       pFilters[kFilter0]->FilterConfig((FilterType)type.get(), freq.get(), 0.f); 
    }
    
    else if (type.get() == kHPF)
    {
        pFilters[kFilter1]->FilterConfig((FilterType)type.get(), freq.get(), 0.f);
    }
    else if (type.get() == kBPF)
    {
       pFilters[kFilter2]->FilterConfig((FilterType)type.get(), freq.get(), freq.get() / Q.get());
    }
    
}

void MultiFilter::setState(const bool shouldBeOn)
{
    activeState = shouldBeOn;
}

bool MultiFilter::getState() const
{
    return activeState.get();
}

void MultiFilter::setFilterType(SInt16 filterSetting)
{
    if (filterSetting == 1)
    {
        type = kLPF;
    }
    else if (filterSetting == 2)
    {
        type = kHPF;
    }
    else if (filterSetting == 3)
    {
        type = kBPF;
    }
}

void MultiFilter::setFilterParam(Float32 freqVal, Float32 qVal, Float32 gainVal)
{
    freq = freqVal;
    Q = qVal;
    gain = gainVal;
}

Float32 MultiFilter::process(Float32 input)
{
    Float32 out = 0.f;
    
    if (activeState.get() == 0)
    {
        
        if (type.get() == kLPF)
        {
            out = pFilters[kFilter0]->Filter(input);
        }
        
        else if (type.get() == kHPF)
        {
            out = pFilters[kFilter1]->Filter(input);
        }
        else if (type.get() == kBPF)
        {
            out = pFilters[kFilter2]->Filter(input);
            out *= gain.get();
        }
        
        return out;
    }
    else
        return input;
    
}
