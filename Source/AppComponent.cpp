/*
 *  AppComponent.cpp
 *  sdaProj
 */

#include "AppComponent.h"

AppComponent::AppComponent() : view (audioMidi)
{
    addAndMakeVisible(&view); 
   
}
AppComponent::~AppComponent()
{
    
}

//ComponentCallbacks============================================================
void AppComponent::resized()
{
    view.setBounds(0, 0, getWidth(), getHeight());
}

void AppComponent::paint (Graphics &g)
{
 
}

//MenuBarCallbacks==============================================================
StringArray AppComponent::getMenuBarNames()
{
	const char* const names[] = { "File", 0 };
	return StringArray (names);
}

PopupMenu AppComponent::getMenuForIndex (int topLevelMenuIndex, const String& menuName)
{
	PopupMenu menu;
	if (topLevelMenuIndex == 0)
        menu.addItem(AudioPrefs, "Audio Prefrences", true, false);
	return menu;
}

void AppComponent::menuItemSelected (int menuItemID, int topLevelMenuIndex)
{
	if (topLevelMenuIndex == FileMenu) 
    {
        if (menuItemID == AudioPrefs) 
        {
            AudioDeviceSelectorComponent audioSettingsComp (audioMidi.getAudioDeviceManager(),
                                                            0, 2, 2, 2,
                                                            true, true, true, true);
            
            audioSettingsComp.setSize (500, 250);
            DialogWindow::showModalDialog ("Audio Settings", &audioSettingsComp, this, Colours::azure, true);
        }
    }
}


