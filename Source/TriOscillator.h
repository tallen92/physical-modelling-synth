//
//  TriOscillator.h
//  sdaAudioMidi
//
//  Created by Tyrone Allen on 25/02/2013.
//
//

#ifndef __sdaAudioMidi__TriOscillator__
#define __sdaAudioMidi__TriOscillator__

#include <iostream>
#include "../JuceLibraryCode/JuceHeader.h"
#include "Oscillator.h"
#include "SimpleFilter.h"

/**
 Generates single Triangle cycle. When used as an excitation type, triangle oscillator does begins to fault at higher frequencies 
 */
class TriOscillator  : public Oscillator

{
public:
	//==============================================================================
    /**
     Constructor
     */
	TriOscillator(); //create base class, make polymorphic
    
    /**
     Destructor
     */
	~TriOscillator();
    //Over ridden functions from the base class============================================================
    /**
    Renders a triangle waveshape whose Frequency is based on  TRIG_LEN  
     */
    float renderWaveShape (const float currentPhase);
    
    /**
    Returns a sample 
    */
    float getSample();
    
private:
	//==============================================================================
    enum
    {
        kFilter0 = 0,
        kNumberOfFilters
    };
    SimpleFilter *pFilters[kNumberOfFilters];
    
};



#endif /* defined(__sdaAudioMidi__TriOscillator__) */
