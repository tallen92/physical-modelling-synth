//
//  TriOscillator.cpp
//  sdaAudioMidi
//
//  Created by Tyrone Allen on 25/02/2013.
//
//

#include "TriOscillator.h"
TriOscillator::TriOscillator()
{
    for(int i = 0; i < kNumberOfFilters; i++)
    {
        pFilters[i] = new SimpleFilter(44100);
    }
    
    pFilters[kFilter0]->FilterConfig(kLPF, 20000.0,0); // to solve anti aliasing problem
}

TriOscillator::~TriOscillator()
{
    for(int i = 0; i < kNumberOfFilters; i++)
	{
		delete pFilters[i];
	}
}

//============================================================
/*
Excitation burst is created each time a note on is detected, as the frequency of the wave will change depending on the TRIG_LEN. 
 */
float TriOscillator::renderWaveShape (const float currentPhase)
{
    incPhase();
    resetPhase();
    
        if (currentPhase < M_PI)
            return pFilters[kFilter0]->Filter((2 / M_PI) * currentPhase - 1);
        else
            return  pFilters[kFilter0]->Filter(3 - (2/ M_PI) * currentPhase);
}


//Passes a sample of noise to the audio callback function
Float32 TriOscillator::getSample()
{
    float output = 0;
    setFrequency(44100/TRIG_LEN.get());
    
    index++;
    if (index >= TRIG_LEN.get())
        index = 0;
    
    if (trig.get() == true)
    {
        trigCount++;
        if (trigCount >= TRIG_LEN.get())
            trig = false;
        
        output = excitation[index] * returnAmplitude();
        
    }
    
    return output;
}