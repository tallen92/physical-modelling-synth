//
//  TDelay.cpp
//  sdaAudioMidi
//
//  Created by Tyrone Allen on 26/02/2013.
//
//

#include "TDelay.h"

TDelay::TDelay()
{
    //iBuffersize = 1000;
    //std::cout<<"size -> "<<iBuffersize<<std::endl;
    
    delayBuffer = new Float32[BUFFER_SIZE];
    
    for(int iPos = 0; iPos < BUFFER_SIZE; iPos++)
		delayBuffer[iPos] = 0;
    
    iBufferWritePos = 0;
    iBufferRightPos = 0;
    decay = 0.f;
    tau = 0.f;
}

TDelay::~TDelay()
{
    delete [] delayBuffer;
}

void TDelay::setDelayInSeconds(Float32 delay)
{
    tau = delay;
}

void TDelay::setDecay (Float32 gain)
{
    decay = gain;
}

Float32 TDelay::getTau() const
{
    return tau;
}

void TDelay::incrPos()
{
    iBufferWritePos++;
    if(iBufferWritePos == BUFFER_SIZE)
        iBufferWritePos = 0;
    
    iBufferRightPos = iBufferWritePos - (tau * SAMPLE_RATE);
    if(iBufferRightPos < 0)
        iBufferRightPos += BUFFER_SIZE;
}

Float32 TDelay::delayLineRead()
{
    
    Float32 out = delayBuffer[iBufferRightPos];
    
    return out;
}

void TDelay::delayLineWrite(Float32 input)
{
    delayBuffer[iBufferWritePos] = input;
}

Float32 TDelay::getDecay() const
{
    return decay;
}

