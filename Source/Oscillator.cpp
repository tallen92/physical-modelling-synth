//
//  Oscillator.cpp
//  sdaAudioMidi
//
//  Created by Tyrone Allen on 25/02/2013.
//
//

#include "Oscillator.h"

Oscillator::Oscillator()
{
    reset();
}

Oscillator::~Oscillator()
{
    
}
void Oscillator::pluck()
{
    for (int i = 0; i < TRIG_LEN.get(); i++)
    {
        excitation[i] = renderWaveShape(returnPhase());
    }
}
void Oscillator::setFrequency (float freq)
{
    frequency = freq;
    phaseInc = (2 * M_PI * frequency.get()) / sampleRate.get();
}

void Oscillator::setNote (int noteNum)
{
    setFrequency (440.f * pow (2, (noteNum - 69) / 12.0));
}

void Oscillator::setAmplitude(float newAmplitude)
{
    amp = newAmplitude/127.0;
}

float Oscillator::returnAmplitude()const
{
    return amp.get();
}

void Oscillator::reset()
{
    for (int i = 0; i < NUM_SAMPLES; i++)
    {
        excitation[i] = 0.f;
    }
    
    phase = 0.f;
    sampleRate = 44100;
    setFrequency (440.f);
    setAmplitude (0.f);
    
    index = 0;
    TRIG_LEN = 441;
    trigCount = 0;
    trig = false;
}

void Oscillator::setSampleRate (float sr)
{
    sampleRate = sr;
    setFrequency (frequency.get());//just to update the phaseInc
}

void Oscillator::trigger()
{
    trigCount = 0;
    trig = true;
    
}

void Oscillator::setTRIG_LEN(int pluckObject)
{
    TRIG_LEN = (int)(pluckObject);
}

float Oscillator::returnPhase()
{
    return phase.get();
}

float Oscillator::returnPhaseInc()
{
    return phaseInc.get();
}

void Oscillator::incPhase()
{
    phase = phase.get() + phaseInc.get();
}

void Oscillator::resetPhase()
{
    if(phase.get() > (2.f * M_PI))
    {
        phase -= (2.f * M_PI);
    }
}

float Oscillator::getFreq()
{
    return frequency.get();
}
