//
//  TremoloGui.cpp
//  sdaAudioMidi
//
//  Created by Tyrone Allen on 25/02/2013.
//
//

#include "TremoloGui.h"

TremoloGui::TremoloGui(AudioMidi& audioMidi_) : audioMidi (audioMidi_)
{
    stateButton.addListener(this);
    addAndMakeVisible(&stateButton);
    stateButton.setColour(TextButton::buttonColourId, Colours::darkblue);
    stateButton.setColour(TextButton::buttonOnColourId, Colours::beige);
    
    RateSlider.addListener(this);
    addAndMakeVisible(&RateSlider);
    RateSlider.setColour(Slider::rotarySliderFillColourId, Colours::darkblue);
    RateSlider.setRange(1.0, 20.0);
    RateSlider.setSliderStyle(Slider::Rotary);
    RateSlider.setTextBoxStyle(juce::Slider::TextEntryBoxPosition(0), false, 0, 0);
    
    DepthSlider.addListener(this);
    addAndMakeVisible(&DepthSlider);
    DepthSlider.setColour(Slider::rotarySliderFillColourId, Colours::darkblue);
    DepthSlider.setRange(0.0, 1.0);
    DepthSlider.setSliderStyle(Slider::Rotary);
    DepthSlider.setTextBoxStyle(juce::Slider::TextEntryBoxPosition(0), false, 0, 0);
    
    Font font("Courier", 12.0, 1);
    Justification justi(36);
    
    addAndMakeVisible(&RateLabel);
    RateLabel.setText("Rate", false);
    RateLabel.setFont(font);
    RateLabel.setJustificationType(justi);
    
    addAndMakeVisible(&DepthLabel);
    DepthLabel.setText("Depth", false);
    DepthLabel.setFont(font);
    DepthLabel.setJustificationType(justi);
}

TremoloGui::~TremoloGui()
{
    
}

//SliderCallbacks===============================================================
void TremoloGui::sliderValueChanged (Slider* slider)
{
    audioMidi.setTremoloParam(RateSlider.getValue(), DepthSlider.getValue());
}
//ButtonCallbacks===============================================================
void TremoloGui::buttonClicked (Button* button)
{
    if (!stateButton.getToggleState())
    {
        RateSlider.setEnabled(false);
        DepthSlider.setEnabled(false);

    }
    else
    {
        RateSlider.setEnabled(true);
        DepthSlider.setEnabled(true);
    }
    
    audioMidi.setTremoloState(!audioMidi.getTremoloState());
    stateButton.setToggleState(audioMidi.getTremoloState(), false);
    
    std::cout<<audioMidi.getTremoloState()<<std::endl;
}

//Component
void TremoloGui::resized()
{
    SInt16 x,y,width,height,sliderSpaceX, labelSpace,bsize;
    
    x = getWidth();
    y = getHeight();
    sliderSpaceX = 75;
    labelSpace = 45;
    width = getWidth()/3.5;
    height = getWidth()/3.5;
    bsize = getWidth()/13;
    
    
    
    RateSlider.setBounds(10, 45, width, height);
    DepthSlider.setBounds(RateSlider.getX() + sliderSpaceX, RateSlider.getY(), width, height);
    
    RateLabel.setBounds(RateSlider.getX(), RateSlider.getY() + labelSpace, width, height/2);
    DepthLabel.setBounds(DepthSlider.getX(), DepthSlider.getY() + labelSpace, width, height/2);
    
    stateButton.setBounds(DepthSlider.getX() + 65, DepthSlider.getY() + 50, bsize, bsize);
    
}
void TremoloGui::paint (Graphics &g)
{
    g.setColour(Colours::darkblue);
    g.setOpacity(0.1);
    g.drawRoundedRectangle(0, 0, this->getWidth(), this->getHeight(), 8, 10);
    g.fillRoundedRectangle(0, 0, this->getWidth(), this->getHeight(), 8);
    
    
}