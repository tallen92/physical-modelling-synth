//
//  ToneControlGui.cpp
//  sdaAudioMidi
//
//  Created by Tyrone Allen on 03/02/2013.
//
//

#include "ToneControlGui.h"

ToneControlGui::ToneControlGui(AudioMidi& audioMidi_) : audioMidi (audioMidi_)
{
   //second change
    pickPosSlider.addListener(this);
    addAndMakeVisible(&pickPosSlider);
    pickPosSlider.setColour(Slider::rotarySliderFillColourId, Colours::darkblue);
    pickPosSlider.setRange(0.001, 0.01);
    pickPosSlider.setSliderStyle(Slider::Rotary);
    pickPosSlider.setTextBoxStyle(juce::Slider::TextEntryBoxPosition(0), false, 0, 0);
    pickPosSlider.setValue(0.004);
    
    StringDampingSlider.addListener(this);
    addAndMakeVisible(&StringDampingSlider);
    StringDampingSlider.setColour(Slider::rotarySliderFillColourId, Colours::darkblue);
    StringDampingSlider.setRange(0.5, 3.0);
    StringDampingSlider.setSliderStyle(Slider::Rotary);
    StringDampingSlider.setTextBoxStyle(juce::Slider::TextEntryBoxPosition(0), false, 0, 0);
    StringDampingSlider.setValue(1.0);
    
    pluckObjectSlider.addListener(this);
    addAndMakeVisible(&pluckObjectSlider);
    pluckObjectSlider.setColour(Slider::rotarySliderFillColourId, Colours::darkblue);
    pluckObjectSlider.setRange(44, 441);
    pluckObjectSlider.setSliderStyle(Slider::Rotary);
    pluckObjectSlider.setTextBoxStyle(juce::Slider::TextEntryBoxPosition(0), false, 0, 0);
    pluckObjectSlider.setValue(400);
   
    excitationType.addListener(this);
    addAndMakeVisible(&excitationType);
    excitationType.setColour(ComboBox::backgroundColourId, Colours::darkcyan);
    excitationType.setColour(ComboBox::arrowColourId, Colours::darkmagenta);
    excitationType.setColour(ComboBox::outlineColourId, Colours::darkcyan);
    excitationType.addItem("Triangle Wave", 1);
    excitationType.addItem("Noise", 2);
    excitationType.setTextWhenNothingSelected("Choose Excitation Type");
    
    Font font("Courier", 12.0, 1);
    Justification justi(36);
    
    addAndMakeVisible(&pickPosLabel);
    pickPosLabel.setText("Pick Postion", false);
    pickPosLabel.setFont(font);
    pickPosLabel.setJustificationType(justi);
    
    addAndMakeVisible(&pluckObjectLabel);
    pluckObjectLabel.setText("Pluck Object", false);
    pluckObjectLabel.setFont(font);
    pluckObjectLabel.setJustificationType(justi);
   
    addAndMakeVisible(&stringDampingLabel);
    stringDampingLabel.setText("String Damping", false);
    stringDampingLabel.setFont(font);
    stringDampingLabel.setJustificationType(justi);
}

ToneControlGui::~ToneControlGui()
{
    
}

//SliderCallbacks===============================================================
void ToneControlGui::sliderValueChanged (Slider* slider)
{
    audioMidi.setToneControlParam(pickPosSlider.getValue(), StringDampingSlider.getValue(), pluckObjectSlider.getValue());
}

void ToneControlGui::comboBoxChanged (ComboBox* comboBoxThatHasChanged)
{
    audioMidi.setExcitationType(excitationType.getSelectedId());
}

//Component
void ToneControlGui::resized()
{
    SInt16 x,y,width,height,sliderSpaceX, labelSpace;
    
    x = getWidth();
    y = getHeight();
    sliderSpaceX = 75;
    labelSpace = 45;
    width = getWidth()/5;
    height = getWidth()/5;
   
    excitationType.setBounds( 10, 10, width * 2, height/2 - 5);
    
    pickPosSlider.setBounds( 10, 45, width, height);
    StringDampingSlider.setBounds(pickPosSlider.getX() + sliderSpaceX, pickPosSlider.getY(), width, height);
    pluckObjectSlider.setBounds(StringDampingSlider.getX() + sliderSpaceX, pickPosSlider.getY(), width, height);
    
    pickPosLabel.setBounds(pickPosSlider.getX(), pickPosSlider.getY() + labelSpace, width, height/2);
    stringDampingLabel.setBounds(StringDampingSlider.getX(), StringDampingSlider.getY() + labelSpace,width, height/2);
    pluckObjectLabel.setBounds(pluckObjectSlider.getX(), pluckObjectSlider.getY() + labelSpace, width, height/2);
    
    
}
void ToneControlGui::paint (Graphics &g)
{
    g.setColour(Colours::darkblue);
    g.setOpacity(0.1);
    g.drawRoundedRectangle(0, 0, this->getWidth(), this->getHeight(), 8, 10);
    g.fillRoundedRectangle(0, 0, this->getWidth(), this->getHeight(), 8);
   

}