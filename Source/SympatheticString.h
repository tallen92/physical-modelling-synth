//
//  SympatheticString.h
//  sdaAudioMidi
//
//  Created by Tyrone Allen on 04/03/2013.
//
//

#ifndef __sdaAudioMidi__SympatheticString__
#define __sdaAudioMidi__SympatheticString__

#include <iostream>
#include "../JuceLibraryCode/JuceHeader.h"
#include "WaveGuideModel.h"
#include <cmath>
/**
 Class is designed to act as a Sympathetic String to the plucked string. A fraction of the output of the plucked string is passed into delay line which is tuned to an overtone of the fundamnetal frequency.
 */
class SympatheticString : public WaveGuideModel
{
public:
    
    /**
     Constructor
     */
    SympatheticString();
    
    /**
     Deconstructor
     */
    ~SympatheticString();
    
    /**
     Configur filter used in resonator
     */
    void filterConfig(SInt16 partial);
    
    /**
     Overridden function that process excitation sample to model a Sympathetic string
     */
    Float32 FilterProcess(Float32 fIn, Float32 pitchBendVal = 1);
    
    
private:
    
};

#endif /* defined(__sdaAudioMidi__SympatheticString__) */
