//
//  ToneControlGui.h
//  sdaAudioMidi
//
//  Created by Tyrone Allen on 03/02/2013.
//
//

#ifndef __sdaAudioMidi__ToneControlGui__
#define __sdaAudioMidi__ToneControlGui__

#include <iostream>
#include "../JuceLibraryCode/JuceHeader.h"
#include "AudioMidi.h"

/**
 Gui class for the WaveGuideModel class, Oscillator class and ExcitationControl class.
 */

class ToneControlGui :  public Component,
                        public Slider::Listener,
                        public ComboBox::Listener
{
public:
    
    ToneControlGui(AudioMidi& audioMidi_);
    
    ~ToneControlGui();
    
    //SliderCallbacks===============================================================
    void sliderValueChanged (Slider* slider);
    
    //ComboBoxCallbacks===============================================================
    void comboBoxChanged (ComboBox* comboBoxThatHasChanged);
    
    //Component
    void resized();
	void paint (Graphics &g);
    
private:
    
    AudioMidi& audioMidi;

    Label pickPosLabel;
    Label stringDampingLabel;
    Label pluckObjectLabel;
    
    ComboBox excitationType;
    
    Slider pickPosSlider;
    Slider StringDampingSlider;
    Slider pluckObjectSlider;
    
};

#endif /* defined(__sdaAudioMidi__ToneControlGui__) */
