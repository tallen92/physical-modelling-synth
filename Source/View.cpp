//
//  View.cpp
//  sdaAudioMidi
//
//  Created by Tyrone Allen on 10/02/2013.
//
//

#include "View.h"

View::View(AudioMidi& audioMidi_) : audioMidi (audioMidi_), toneCtrlGui (audioMidi), bodyGui (audioMidi), filterGui(audioMidi),tremGui(audioMidi), masterCtrlGui(audioMidi)
{
    addAndMakeVisible(&toneCtrlGui);
    addAndMakeVisible(&bodyGui);
    addAndMakeVisible(&filterGui);
    addAndMakeVisible(&tremGui);
    addAndMakeVisible(&masterCtrlGui);
    
    addAndMakeVisible(&toneCtrlLabel);
    addAndMakeVisible(&bodyLabel);
    addAndMakeVisible(&filterlabel);
    addAndMakeVisible(&tremlabel);
    addAndMakeVisible(&masterCtrlLabel);
    
    addAndMakeVisible(&titleLbl);
    
    toneCtrlLabel.setLabelText("Tone Control");
    bodyLabel.setLabelText("Body");
    filterlabel.setLabelText("Filter");
    tremlabel.setLabelText("Tremolo");
    masterCtrlLabel.setLabelText("Master Controls");
}

View::~View()
{
    
}
//ComponentCallbacks============================================================
void View::resized()
{
    int paramWidth = getHeight()/2 - 25;
    int paramHeight = getWidth()/5;
    
    int spaceBeside = 40;
    int spaceBelow = 70;
    
    int labelSpace = 35;
    
    
    toneCtrlGui.setBounds(20, 60, paramWidth, paramHeight);
    toneCtrlLabel.setBounds(toneCtrlGui.getX(), toneCtrlGui.getY() - labelSpace, paramWidth, paramHeight/4);
    
    
    bodyGui.setBounds(toneCtrlGui.getX() + toneCtrlGui.getWidth() + spaceBeside,
                      toneCtrlGui.getY(), paramWidth, paramHeight);
    bodyLabel.setBounds(bodyGui.getX(), bodyGui.getY() - labelSpace, paramWidth, paramHeight/4);
    
    
    filterGui.setBounds(toneCtrlGui.getX(), toneCtrlGui.getY() + toneCtrlGui.getHeight() + spaceBelow,
                        paramWidth, paramHeight);
    filterlabel.setBounds(filterGui.getX(), filterGui.getY() - labelSpace, paramWidth, paramHeight/4);
    
    
    tremGui.setBounds(filterGui.getX() + filterGui.getWidth() + spaceBeside,
                      bodyGui.getY() + bodyGui.getHeight() + spaceBelow, paramWidth/1.5, paramHeight);
    tremlabel.setBounds(tremGui.getX(), tremGui.getY() - labelSpace, paramWidth/1.5, paramHeight/4);
    
    
    masterCtrlGui.setBounds(filterGui.getX(), filterGui.getY() + filterGui.getHeight() + spaceBelow,
                            paramWidth/1.5, paramHeight);
    masterCtrlLabel.setBounds(masterCtrlGui.getX(), masterCtrlGui.getY() - labelSpace,
                              paramWidth/1.5, paramHeight/4);
    
    titleLbl.setBounds(masterCtrlLabel.getX() + 300, masterCtrlLabel.getY() - 70, getWidth()/2, paramWidth);
    
}

void View::paint (Graphics &g)
{
    
    JPEGImageFormat cache;
    
    ImageComponent img;
    
    File file("/Volumes/External storage device/Physical Modelling Synth/sdaAudioMidi/Source/background.jpg");
    
    File imgFile(file.getRelativePathFrom(file.getCurrentWorkingDirectory()));
    
    g.drawImage(cache.loadFrom(imgFile),
                this->getX(), this->getY(),this->getWidth(), this->getHeight(),
                this->getX(), this->getY(),this->getWidth(), this->getHeight());
}