//
//  MultiFilter.h
//  sdaAudioMidi
//
//  Created by Tyrone Allen on 27/02/2013.
//
//

#ifndef __sdaAudioMidi__MultiFilter__
#define __sdaAudioMidi__MultiFilter__

#include <iostream>
#include "../JuceLibraryCode/JuceHeader.h"
#include "SimpleFilter.h"

/**
 Class that allows LP,HP, or BP filtering to be applied to the instrument
 */

class MultiFilter
{
    
public:
    
    /**
     Constructor
     */
    MultiFilter();
    
    /**
     Deconstructor
     */
    ~MultiFilter();
    
    /**
     Configures filter with neccessary filter type
     */
    void configfilter();
    
    /**
     Sets the type of filter to be used
     */
    void setFilterType(SInt16 filterSetting );
    
    /**
     Sets filter parameters
     */
    void setFilterParam(Float32 freqVal, Float32 qVal=0, Float32 gainVal=0);
    
    /**
     Filters input sample
     */
    Float32 process(Float32 input);
    
    /**
     Sets the active state of the filter
     */
    void setState(const bool shouldBeOn);
    
    /**
     Returns the active state of the filter
     */
    bool getState() const;
    
private:
    
    //simple filters
    //Filters
    enum
    {
        kFilter0 = 0,
        kFilter1,
        kFilter2,
        kNumberOfFilters
    };
    
    SimpleFilter *pFilters[kNumberOfFilters];
    
    CriticalSection sharedMemory;
    
    Atomic<int>type;
    Atomic<float> freq;
    Atomic<float> gain;
    Atomic<float> Q;
    Atomic<int> activeState;
    
};

#endif /* defined(__sdaAudioMidi__MultiFilter__) */
