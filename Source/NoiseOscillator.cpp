//
//  SinOscillator.cpp
//  sdaAudioMidi
//
//  Created by Tyrone Allen on 13/11/2012.
//
//

#include "NoiseOscillator.h"

NoiseOscillator::NoiseOscillator()
{
     // seed based on current time
     unsigned int value = (unsigned int) time (NULL);
    
    for (int i = 0; i < NUM_SAMPLES; i++)
    {
        //linear congruential generator adapted from project created by Martin Robinson
        
        // next random number
        value = RNG_MAGIC1 * value + RNG_MAGIC2;
        
        // mask into a valid floating point number bit pattern
        const unsigned int floatBits = RNG_FLOAT_ONE | (RNG_FLOAT_ONEMASK & value);
        
        // .. but it is in the range 1...2
        const float floatValue1_2 = *(float*)(&floatBits);
        
        // so, normalise to -1...1
        const float normalisedFloat = floatValue1_2 * 2.f - 3.f;
        
       //assign normalisedFloat to element in array
        
        excitation[i] = normalisedFloat;
        
    }
}

NoiseOscillator::~NoiseOscillator()
{
    
}

//ComponentCallbacks============================================================

float NoiseOscillator::renderWaveShape (const float currentPhase)
{
    
    /*
    When noise generation algorithm is implemented in this function, 
    the algorithm fails so the algorithm is placed in the contructor
         */
    return 0;
}

/*
Passes a sample of noise to the audio callback function.
If the amount of samples pass reaches the TRIG_LEN then only 0's will be outputted until next call from the incoming midi message.
 */
Float32 NoiseOscillator::getSample()
{    
    float output = 0;
    index++;
    if (index >= NUM_SAMPLES)
        index = 0;
    
    if (trig.get() == true)
    {
        trigCount++;
        if (trigCount >= TRIG_LEN.get())
            trig = false;
        
        output = excitation[index] * returnAmplitude();
    }
    
    return output;
}

