//
//  Oscillator.h
//  sdaAudioMidi
//
//  Created by Tyrone Allen on 25/02/2013.
//
//

#ifndef __sdaAudioMidi__Oscillator__
#define __sdaAudioMidi__Oscillator__

#include <iostream>
#include <cmath>
#include "../JuceLibraryCode/JuceHeader.h"

#define NUM_SAMPLES 44100

#define RNG_MAGIC1            ((unsigned int)1664525)
#define RNG_MAGIC2            ((unsigned int)1013904223)
#define RNG_FLOAT_ONE         0x3f800000
#define RNG_FLOAT_ONEMASK     0x007fffff

/**
 Abstract base class for a oscillators that can be used as a form of excitation for the model or as an LFO for modulation effects
 */

class Oscillator
{
public:
    //==============================================================================
    /**
     Oscillator constructor
     */
    Oscillator();
    
    /**
     Oscillator destructor
     */
    virtual ~Oscillator();
    
    /**
     sets the frequency of the oscillator
     */
    void setFrequency (float freq);
    
    /**
     sets frequency using a midi note number
     */
    void setNote (int noteNum);
    
    /**
     Returns the frequency of the oscillator
     */
    float getFreq();
    
    /**
     sets the amplitude of the oscillator
     */
    void setAmplitude(float newAmplitude);
    
    /**
     returns the amplitude of the oscillator
     */
    float returnAmplitude()const;
    
    /**
     resets the oscillator
     */
    void reset();
    
    /**
     sets the sample rate
     */
    void setSampleRate (float sr);
    
    /**
     Calls the function renderWaveShape to generate an excitation to the system. Only used by non-noise type oscillators.
     */
    void pluck();
    
    /**
     When called allows a burst of the excitation to trigger the waveguide model.
     */
    void trigger();
    
    /**
     Sets the length of the excitation burst, altering the apparent excitation object
     */
    void setTRIG_LEN(int pluckObject);
    
    /**
     Returns the phase of the waveshape.
     */
    
    float returnPhase();
    
    /**
     Returns the phase increment of the waveshape.
     */
    float returnPhaseInc();
    
    /**
     Increments the phase of the waveshape
     */
    void incPhase();
    
    /**
     Resets the waveshape phase
     */
    void resetPhase();
    
    /**
     pure virtual function for derived classes to provide the execution of the waveshape
     */
    virtual float renderWaveShape (const float currentPhase = 0) = 0;
    
    /**
     pure virtual function for derived classes to return the next sample
     */
    virtual float getSample() = 0;
    
protected:
    float excitation[NUM_SAMPLES];
    Atomic<int> TRIG_LEN;
    Atomic<int> trig;
    int index;
    SInt16 trigCount;
    
private:
    //all atomic for thread safety
    Atomic<float> frequency;
    Atomic<float> amp;
    Atomic<float> sampleRate;
    Atomic<float> phase;
    Atomic<float> phaseInc;
    
    
};

#endif /* defined(__sdaAudioMidi__Oscillator__) */
