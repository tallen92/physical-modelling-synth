/*
 *  JuceApp.h
 *  sdaProj
 */

#ifndef H_JUCEAPP
#define H_JUCEAPP

#include "../JuceLibraryCode/JuceHeader.h"
#include "AppWindow.h"

/**
The top level of the applicaiton 
 */

class JuceApp  : public JUCEApplication
{
public:
    //==========================================================================
    /**
     Constructor
     */
    JuceApp();
    
    /**
     Destructor
     */
    ~JuceApp();
    
    //Applicaiton callbacks=====================================================
    void initialise (const String& commandLine);
    void shutdown();
    //==========================================================================
    void systemRequestedQuit();
    //==========================================================================
    const String getApplicationName();
    const String getApplicationVersion();
    bool moreThanOneInstanceAllowed();
    void anotherInstanceStarted(const String& commandLine);
	
private:
    AppWindow* appWindow;
};

#endif //H_JUCEAPP