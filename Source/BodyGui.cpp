//
//  BodyGui.cpp
//  sdaAudioMidi
//
//  Created by Tyrone Allen on 02/03/2013.
//
//

#include "BodyGui.h"

BodyGui::BodyGui(AudioMidi& audioMidi_) : audioMidi (audioMidi_)
{
    lowSlider.addListener(this);
    addAndMakeVisible(&lowSlider);
    lowSlider.setColour(Slider::rotarySliderFillColourId, Colours::darkblue);
    lowSlider.setRange(80.0, 200.0);
    lowSlider.setSliderStyle(Slider::Rotary);
    lowSlider.setTextBoxStyle(juce::Slider::TextEntryBoxPosition(0), false, 0, 0);
    lowSlider.setValue(80.0);
    
    midSlider.addListener(this);
    addAndMakeVisible(&midSlider);
    midSlider.setColour(Slider::rotarySliderFillColourId, Colours::darkblue);
    midSlider.setRange(200.0, 800.0);
    midSlider.setSliderStyle(Slider::Rotary);
    midSlider.setTextBoxStyle(juce::Slider::TextEntryBoxPosition(0), false, 0, 0);
    midSlider.setValue(300.0);
    
    highSlider.addListener(this);
    addAndMakeVisible(&highSlider);
    highSlider.setColour(Slider::rotarySliderFillColourId, Colours::darkblue);
    highSlider.setRange(800, 6000.0);
    highSlider.setSliderStyle(Slider::Rotary);
    highSlider.setTextBoxStyle(juce::Slider::TextEntryBoxPosition(0), false, 0, 0);
    highSlider.setValue(850.0);
    
    Font font("Courier", 12.0, 1);
    Justification justi(36);
        
    addAndMakeVisible(&lowLabel);
    lowLabel.setText("Low", false);
    lowLabel.setFont(font);
    lowLabel.setJustificationType(justi);
    
    addAndMakeVisible(&midLabel);
    midLabel.setText("Mid", false);
    midLabel.setFont(font);
    midLabel.setJustificationType(justi);
    
    addAndMakeVisible(&highLabel);
    highLabel.setText("High", false);
    highLabel.setFont(font);
    highLabel.setJustificationType(justi);

    
}

BodyGui::~BodyGui()
{
    
}

//SliderCallbacks===============================================================
void BodyGui::sliderValueChanged (Slider* slider)
{
    audioMidi.setBodyParam(lowSlider.getValue(), midSlider.getValue(), highSlider.getValue());
}


//Component
void BodyGui::resized()
{
    SInt16 x,y,width,height,sliderSpaceX, labelSpace;
    
    x = getWidth();
    y = getHeight();
    sliderSpaceX = 75;
    labelSpace = 45;
    width = getWidth()/5;
    height = getWidth()/5;
    
    lowSlider.setBounds( 10, 45, width, height);
    midSlider.setBounds(lowSlider.getX() + sliderSpaceX, lowSlider.getY(), width, height);
    highSlider.setBounds(midSlider.getX() + sliderSpaceX, midSlider.getY(), width, height);
    
    lowLabel.setBounds(lowSlider.getX(), lowSlider.getY() + labelSpace,width, height/2);
    midLabel.setBounds(midSlider.getX(), midSlider.getY() + labelSpace,width, height/2);
    highLabel.setBounds(highSlider.getX(), highSlider.getY() + labelSpace, width, height/2);
    
}
void BodyGui::paint (Graphics &g)
{
    g.setColour(Colours::darkblue);
    g.setOpacity(0.1);
    g.drawRoundedRectangle(0, 0, this->getWidth(), this->getHeight(), 8, 10);
    g.fillRoundedRectangle(0, 0, this->getWidth(), this->getHeight(), 8);
     
}