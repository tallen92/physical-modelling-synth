//
//  WaveGuideModel.h
//  sdaAudioMidi
//
//  Created by Tyrone Allen on 21/12/2012.
//
//

#ifndef __sdaAudioMidi__WaveGuideModel__
#define __sdaAudioMidi__WaveGuideModel__

#include <iostream>
#include "../JuceLibraryCode/JuceHeader.h"
#include "SimpleFilter.h"
#include "AllPassFilter.h"

#define BUFFER_SIZE 1000

/**
 An abstract base class for a waveguide resonator that is designed to physically model a plucked string instrument based on the Karplus Strong algorithm.
 The standing wave generation and pitch of the string is modelled by a feedback comb filter.
 The reduction of high frequenices as the sound of the plucked string decays is modelled by a two pole filter and the damping of the string is modelled by a gain coefficient mapped from the fundamental frequency of the string.
 */

class WaveGuideModel

{
public:
	//==============================================================================
    /**
     Constructor dynamically allocates memory for a two pole filter and the comb filter.
     */
	WaveGuideModel();
    
    /**
     Destructor
     */
	virtual ~WaveGuideModel();
    
    /**
     Sets the pitch of the string as the length of the delay line is tau * sampling rate. Also sets the gain coefficient of the system
     */
    void setTau(Float32 freq);
    
    /**
     Returns the time period of the string
     */
    Float32 returnTau ()const;
    
    /**
     Sets the time it takes the string to decay to -60Db
     */
    void setRVT (Float32 tension);
    
    /**
     Returns the gain coefficient of the system
     */
    Float32 getGDecay()const;
    
    /**
     Set the decay of the all pass filter
     */
    void setAPDecay (Float32 gain);  
    
    /**
     Set the delay time in seconds of the all pass filter
     */
    void setAPTau (Float32 delay);
    
    /**
     pure virtual function configures the two pole filter of the model
     */
    virtual void filterConfig(SInt16 partial) = 0;
    
    /**
     pure virtual function for the main process of the model
     
     @param                     fin: sample value of excitation
     pitchBendVal: value to increase the delay length by
     */
    virtual Float32 FilterProcess(Float32 fIn, Float32 pitchBendVal = 1.0) = 0;
    
protected:
    
    /**
     increment and reset delay line read/write variables
     */
    void incrPos(Float32 pitchBendVal = 1);
    
    //simple filters
    enum
    {
        kFilter0 = 0,
        kFilter1,
        kFilter2,
        kNumberOfFilters
    };
    
    SimpleFilter *pFilters[kNumberOfFilters];
    
    AllPassFilter APFilter;
    
    //Primitives
    Float32 *pfCircularBuffer;
    Float32 fSR;
    SInt32 iBufferWritePos;
    SInt16 iBufferReadPos;
    
    Atomic<Float32>RVT;
    
private:
	//==============================================================================
    
    Atomic<Float32> tau;
    Atomic<Float32> gDecay;
};

#endif /* defined(__sdaAudioMidi__WaveGuideModel__) */
