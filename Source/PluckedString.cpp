//
//  PluckedString.cpp
//  sdaAudioMidi
//
//  Created by Tyrone Allen on 04/03/2013.
//
//

#include "PluckedString.h"

PluckedString::PluckedString()
{
    
}

PluckedString::~PluckedString()
{
    
}

void PluckedString::filterConfig(SInt16 partial)
{
    pFilters[kFilter1]->FilterConfig(kLPF, 1000.0 * partial, 0.0);
}

Float32 PluckedString::FilterProcess(Float32 fIn, Float32 pitchBendVal)
{
    Float32 fOut = 0.f;
    
    incrPos(pitchBendVal);
    
    fOut = pfCircularBuffer[iBufferReadPos] * getGDecay();
    
    fOut = APFilter.filter(fOut);
    
    fOut = pFilters[kFilter1]->Filter(fOut);
    
    fOut += fIn;
    
    pfCircularBuffer[iBufferWritePos] = fOut;
    
    return fOut;
}
