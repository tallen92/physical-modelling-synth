//
//  SineOscillator.cpp
//  sdaAudioMidi
//
//  Created by Tyrone Allen on 25/02/2013.
//
//

#include "SineOscillator.h"

SineOscillator::SineOscillator()
{
    
}

SineOscillator::~SineOscillator()
{
    
}

//ComponentCallbacks============================================================
float SineOscillator::renderWaveShape (const float currentPhase)
{
    float out = sinf(currentPhase);
    
    return out;
}


//Passes a sample of noise to the audio callback function
Float32 SineOscillator::getSample()
{
    incPhase();
    resetPhase();
    float out = renderWaveShape (returnPhase());
   
    return out;
}