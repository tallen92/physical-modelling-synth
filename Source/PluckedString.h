//
//  PluckedString.h
//  sdaAudioMidi
//
//  Created by Tyrone Allen on 04/03/2013.
//
//

#ifndef __sdaAudioMidi__PluckedString__
#define __sdaAudioMidi__PluckedString__

#include <iostream>
#include "../JuceLibraryCode/JuceHeader.h"
#include "WaveGuideModel.h"
#include <cmath>

/**
 Models a plucked string instrument. Inherits the WaveGuideModel base class
 */
class PluckedString : public WaveGuideModel
{
public:
    
    /**
     Constructor
     */
    PluckedString();
    
    /**
     Deconstructor
     */
    ~PluckedString();
    
    /**
     Configur filter used in resonator
     */
    void filterConfig(SInt16 partial);
    
    /**
     Overridden function that process excitation sample to model a vibrating plucked string
     */
    Float32 FilterProcess(Float32 fIn, Float32 pitchBendVal = 1);
    
private:
    
};

#endif /* defined(__sdaAudioMidi__PluckedString__) */
