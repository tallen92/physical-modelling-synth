//
//  DynamicLevelFilter.cpp
//  sdaAudioMidi
//
//  Created by Tyrone Allen on 27/02/2013.
//
//

#include "DynamicLevelFilter.h"

DynamicLevelFilter::DynamicLevelFilter()
{
    setDelayInSeconds(1/44100);
}

DynamicLevelFilter::~DynamicLevelFilter()
{
    
}

Float32 DynamicLevelFilter::filter(Float32 input)
{
    //y[n] = a0 * x[n] - b1 * y[n-1]
    //l * l0(l) * x(n) + (1-L) * y(n)
    
    float delayLineOutput = delayLineRead();
    
    float LO = powf(getDecay(), 1/3);
	
	//float output = (1 + getDecay()) * input - getDecay() * delayLineOutput;
    
    float output = getDecay() * LO * input + (1 - getDecay()) * delayLineOutput;
	
	delayLineWrite(output);
	
	return output;
}