//
//  BodyGui.h
//  sdaAudioMidi
//
//  Created by Tyrone Allen on 02/03/2013.
//
//

#ifndef __sdaAudioMidi__BodyGui__
#define __sdaAudioMidi__BodyGui__

#include <iostream>
#include "../JuceLibraryCode/JuceHeader.h"
#include "AudioMidi.h"

/**
 Gui class for the BodyResonance class.
 */

class BodyGui : public Component,
                public Slider::Listener

{
public:
    
    BodyGui(AudioMidi& audioMidi_);
    
    ~BodyGui();
    
    //SliderCallbacks===============================================================
    void sliderValueChanged (Slider* slider);
    
    //Component
    void resized();
	void paint (Graphics &g);
    
private:
    
    AudioMidi& audioMidi;

    Label lowLabel;
    Label midLabel;
    Label highLabel;
    
    Slider lowSlider;
    Slider midSlider;
    Slider highSlider;
    
};
#endif /* defined(__sdaAudioMidi__BodyGui__) */
