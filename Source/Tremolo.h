//
//  Tremolo.h
//  sdaAudioMidi
//
//  Created by Tyrone Allen on 19/02/2013.
//
//

#ifndef __sdaAudioMidi__Tremolo__
#define __sdaAudioMidi__Tremolo__

#include <iostream>
#include "../JuceLibraryCode/JuceHeader.h"
#include "SineOscillator.h"

/**
Class that generates a low frequency oscillator for use as tremolo affect on the string model. 
 */

class Tremolo 
{
public:
    
    /**
     Constructor
     */
    Tremolo();
    
    /**
     Deconstructor
     */
    ~Tremolo();
    
    /**
    Sets rate and depth of LFO 
     */
    void setRateDepth(Float32 rate, Float32 depth);
    
    /**
    Generates sine wave to use LFO 
    */
    Float32 modulation();
    
    /**
    Sets active state of tremolo object
     */
    void setState(const bool shouldBeOn);
    
    /**
     Returns active state of tremolo object
     */
    bool getState() const;
    
private:
    
    SineOscillator sinOsc;
    Atomic<float>fRate;
    Atomic<float>fDepth;
    Atomic<int>state;
    

    
    
};

#endif /* defined(__sdaAudioMidi__Tremolo__) */
