//
//  SineOscillator.h
//  sdaAudioMidi
//
//  Created by Tyrone Allen on 25/02/2013.
//
//

#ifndef __sdaAudioMidi__SineOscillator__
#define __sdaAudioMidi__SineOscillator__

#include <iostream>
#include <iostream>
#include "../JuceLibraryCode/JuceHeader.h"
#include "Oscillator.h"

/**
 Generates a Sine wave 
 */
class SineOscillator  : public Oscillator

{
public:
	//==============================================================================
    /**
     Constructor
     */
	SineOscillator(); 
    
    /**
     Destructor
     */
	~SineOscillator();
    //ComponentCallbacks============================================================
    /**
     Renders a sinusoidal waveshape
     */
    float renderWaveShape (const float currentPhase);
    
    /**
     Returns a sample
     */
    float getSample();
    
private:
	//==============================================================================
    
    
};



#endif /* defined(__sdaAudioMidi__SineOscillator__) */
