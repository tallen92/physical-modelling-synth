//
//  MasterControlGui.cpp
//  sdaAudioMidi
//
//  Created by Tyrone Allen on 10/03/2013.
//
//

#include "MasterControlGui.h"

MasterControlGui::MasterControlGui(AudioMidi& audioMidi_) : audioMidi (audioMidi_)
{
    addAndMakeVisible(&gainSlider);
    gainSlider.setSliderStyle(Slider::LinearVertical);
    gainSlider.setRange(0.0, 1.0, 0.01);
    gainSlider.addListener(this);
    
    addAndMakeVisible(&meterSlider);
    meterSlider.setSliderStyle(Slider::LinearBar);
    meterSlider.setRange(0.0, 1.0, 0.01);
    meterSlider.addListener(this);
    
    Font font("Courier", 12.0, 1);
    Justification justi(33);
    
    addAndMakeVisible(&gainLabel);
    gainLabel.setText("Volume", false);
    gainLabel.setFont(font);
    gainLabel.setJustificationType(justi);
    
    addAndMakeVisible(&meterLabel);
    meterLabel.setText("Level", false);
    meterLabel.setFont(font);
    meterLabel.setJustificationType(justi);
    
    audioMidi.addActionListener(this);
    startTimer(70); // meter value
    
    
}

MasterControlGui::~MasterControlGui()
{
    
}

//TimerCallbacks===============================================================
void MasterControlGui::timerCallback (void)
{
    if (audioMidi.getMeterValue() < 0.65)
    {
        meterSlider.setColour(Slider::thumbColourId, Colours::green);
    }

    else if (audioMidi.getMeterValue() > 0.65 && audioMidi.getMeterValue() < 0.9)
    {
        meterSlider.setColour(Slider::thumbColourId, Colours::yellow);
    }

    else
    {
        meterSlider.setColour(Slider::thumbColourId, Colours::red);
    }
    meterSlider.setValue(audioMidi.getMeterValue());
}

//SliderCallbacks===============================================================
void MasterControlGui::sliderValueChanged (Slider* slider)
{
        if (slider == &gainSlider)
        {
            sharedMemory.enter();
            Float32 newGain = gainSlider.getValue();
            newGain = newGain * newGain * newGain;
            audioMidi.setGainValue(newGain);
            sharedMemory.exit();
        }
}

//ActionListenerCallback===========================================
void MasterControlGui::actionListenerCallback (const String& message)
{
    if(message.startsWith("gain"))
    {
        String subString = message.fromFirstOccurrenceOf(":", false, true);
        float value = subString.getFloatValue();
        gainSlider.setValue(value);
    }
    
}
//Component
void MasterControlGui::resized()
{
    SInt16 x,y,width,height,sliderSpaceX, labelSpace;
    
    x = getWidth();
    y = getHeight();
    sliderSpaceX = 75;
    labelSpace = 45;
    width = getWidth()/5;
    height = getWidth()/5;
    
    gainSlider.setBounds(10,15, 20, getHeight()/1.5);
    meterSlider.setBounds(gainSlider.getX() + 70,gainSlider.getY(), 20, getHeight()/1.5);
    
    gainLabel.setBounds(gainSlider.getX(),gainSlider.getY() + 60, getWidth()/2, getHeight()/2);
    meterLabel.setBounds(meterSlider.getX() - 5, meterSlider.getY() + 60, getWidth()/2, getHeight()/2);
}
void MasterControlGui::paint (Graphics &g)
{
    g.setColour(Colours::darkblue);
    g.setOpacity(0.1);
    g.drawRoundedRectangle(0, 0, this->getWidth(), this->getHeight(), 8, 10);
    g.fillRoundedRectangle(0, 0, this->getWidth(), this->getHeight(), 8);
    
}