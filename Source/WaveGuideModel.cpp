//
//  WaveGuideModel.cpp
//  sdaAudioMidi
//
//  Created by Tyrone Allen on 21/12/2012.
//
//

#include "WaveGuideModel.h"
#include <cmath>

WaveGuideModel::WaveGuideModel()
{
    for(int i = 0; i < kNumberOfFilters; i++)
    {
        pFilters[i] = new SimpleFilter(44100);
    }
    
	pfCircularBuffer = new Float32[BUFFER_SIZE];
	
	for(int iPos = 0; iPos < BUFFER_SIZE; iPos++)
		pfCircularBuffer[iPos] = 0;
	
	iBufferWritePos = 0;
    iBufferReadPos = 0;
    tau = 0.f;
    fSR = 44100.00;
    RVT = 1.0;
    gDecay = 0.f;
}

WaveGuideModel::~WaveGuideModel()
{
    for(int i = 0; i < kNumberOfFilters; i++)
	{
		delete pFilters[i];
	}

    delete [] pfCircularBuffer;
}

void WaveGuideModel::setTau(Float32 freq) 
{
    if ((1 / freq) * fSR >= 0.f && (1 / freq) * fSR <= BUFFER_SIZE -1 )// protect against error
    {
        tau = 1.0 / freq;
        if (powf(0.001,tau.get()/RVT.get()) >= 0 && powf(0.001,tau.get()/RVT.get()) < 1.0)// protect against error
        {
            gDecay= powf(0.001,tau.get()/RVT.get()); // equation makes sure the decay of the string is proportional to the frequency
        }
    }
    else
    {
        std::cout<<"Freq outside range"<<std::endl;
    }
}

Float32 WaveGuideModel::returnTau ()const
{
    return tau.get();
}

void WaveGuideModel::setRVT (Float32 tension)
{
    if (tension >= 0.5 && tension <= 3.0)
        RVT = tension;//Set damping time of decay
}

Float32 WaveGuideModel::getGDecay()const
{
    return gDecay.get();
}

void WaveGuideModel::setAPDecay (Float32 gain)
{
    APFilter.setDecay(gain);
}

void WaveGuideModel::setAPTau (Float32 delay)
{
    APFilter.setDelayInSeconds(delay);
}

void WaveGuideModel::incrPos(Float32 pitchBendVal) // PITCH may be VOLATILE
{
    iBufferWritePos++;
    if(iBufferWritePos == BUFFER_SIZE)
        iBufferWritePos = 0;
    
    iBufferReadPos = iBufferWritePos - (fSR / ((1/(tau.get() - APFilter.getTau())) * pitchBendVal)); // minus all pass filter delay
    if(iBufferReadPos < 0)
        iBufferReadPos += BUFFER_SIZE;
    
}