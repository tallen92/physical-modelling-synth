//
//  ExcitationControl.cpp
//  sdaAudioMidi
//
//  Created by Tyrone Allen on 26/01/2013.
//
//

#include "ExcitationControl.h"

ExcitationControl::ExcitationControl()
{
	pfCircularBuffer2 = new Float32[BUFFER_SIZE];
	
	for(int iPos = 0; iPos < BUFFER_SIZE; iPos++)
		pfCircularBuffer2[iPos] = 0;
	
	iBufferWritePos2 = 0;
    pickPos = 0.005;
    fSR2 = 44100.0;
    
    DLFilter.setDecay(0.5);
    
}

ExcitationControl::~ExcitationControl()
{
    
    delete [] pfCircularBuffer2;
}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
void ExcitationControl::pickVelConfig(Float32 amp)
{    
    Float32 vel = 0;
    
    vel = amp / 127.0;
    
    vel = vel * vel * vel;
    
    DLFilter.setDecay(vel);
}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
void ExcitationControl::setPickPos (Float32 pos)
{
    pickPos = pos;
}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Float32 ExcitationControl::pickVelocity (Float32 fIn)
{
    Float32 output = 0.f;
    
    output = DLFilter.filter(fIn);
    
    return output;
}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Float32 ExcitationControl::pickPosition (Float32 fexcite)
{
    Float32 fOut = 0.f;
    SInt16 iBufferReadPos = 0;
    
    iBufferWritePos2++;
    if(iBufferWritePos2 == BUFFER_SIZE)
        iBufferWritePos2 = 0;
    pfCircularBuffer2[iBufferWritePos2] = fexcite;
    
    iBufferReadPos = iBufferWritePos2 - (pickPos.get() * fSR2);
    if(iBufferReadPos < 0)
        iBufferReadPos += BUFFER_SIZE;
    
    fOut = pfCircularBuffer2[iBufferReadPos] * -1.0;
    
    fOut += fexcite;
    
    return fOut;
}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Float32 ExcitationControl::exciteProcess (Float32 fIn)
{
    Float32 fexcite;
    
     fexcite = pickVelocity(fIn);
    
     fexcite = pickPosition(fexcite);
    
    return fexcite;
}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~