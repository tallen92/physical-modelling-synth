//
//  TDelay.h
//  sdaAudioMidi
//
//  Created by Tyrone Allen on 26/02/2013.
//
//

#ifndef __sdaAudioMidi__TDelay__
#define __sdaAudioMidi__TDelay__

#include <iostream>
#include "../JuceLibraryCode/JuceHeader.h"

#define SAMPLE_RATE 44100
#define BUFFER_SIZE 1000

/**
 Abstract base class for a delay line type filter
 */
class TDelay
{
public:
    
    /**
	 Constructor
	 */
    TDelay();
    
    /**
	 Deconstructor
	 */
    virtual ~TDelay();
    
    /**
     Set the delay time in seconds of the delayline
     */
    void setDelayInSeconds(Float32 delay);
    
    /**
     returns inputted delay time in seconds
     */
    Float32 getTau() const;
    
    /**
     Sets the decay
	 */
    void setDecay (Float32 gain);
    
    /**
     Pure virtual function to perform the filter processing
	 */
    virtual Float32 filter(Float32 input)=0;
    
protected:
    
    /**
     Increments read/write postions of delay line
     */
    void incrPos();
    
    /**
     Reads out values from delay line, used only in filter function.
     */
    Float32 delayLineRead();
    
    /**
     Writes in values from delay line, used only in filter function.
     */
    void delayLineWrite(Float32 input);
    
    /**
     Returns decay value
     */
    Float32 getDecay() const;
    
private:
    
    Float32 *delayBuffer;
    SInt32 iBufferWritePos;
    SInt16 iBufferRightPos;
    Float32 tau;
    Float32 decay;
};


#endif /* defined(__sdaAudioMidi__TDelay__) */
