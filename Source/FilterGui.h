//
//  FilterGui.h
//  sdaAudioMidi
//
//  Created by Tyrone Allen on 08/02/2013.
//
//

#ifndef __sdaAudioMidi__FilterGui__
#define __sdaAudioMidi__FilterGui__

#include <iostream>
#include "../JuceLibraryCode/JuceHeader.h"
#include "AudioMidi.h"

/**
Gui class for the multifilter class 
 */

class FilterGui :   public Component,
                    public Slider::Listener,
                    public ComboBox::Listener,
                    public Button::Listener
{
public:
    
    FilterGui(AudioMidi& audioMidi_);
    
    ~FilterGui();
    
    //SliderCallbacks===============================================================
    void sliderValueChanged (Slider* slider);
    
    //ComboBoxCallbacks===============================================================
    void comboBoxChanged (ComboBox* comboBoxThatHasChanged);
    
    //ButtonCallbacks===============================================================
    void buttonClicked (Button* button);
    
    void resized();
    
	void paint (Graphics &g);
    
private:
    
    AudioMidi& audioMidi;

    ComboBox filterMode;
    
    Slider frequencySlider;
    Slider qSlider;
    Slider gainSlider;
    
    Label filterSettingLabel;
    
    Label frequencyLabel;
    Label qLabel;
    Label gainLabel;
    
    TextButton stateButton; //set toggle state what ever object state is, false
    
};

#endif /* defined(__sdaAudioMidi__FilterGui__) */
