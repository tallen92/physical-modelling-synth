//
//  FilterGui.cpp
//  sdaAudioMidi
//
//  Created by Tyrone Allen on 08/02/2013.
//
//

#include "FilterGui.h"

FilterGui::FilterGui(AudioMidi& audioMidi_) : audioMidi (audioMidi_)
{
    frequencySlider.addListener(this);
    addAndMakeVisible(&frequencySlider);
    frequencySlider.setColour(Slider::rotarySliderFillColourId, Colours::darkblue);
    frequencySlider.setRange(80.0, 2000.0);
    frequencySlider.setSliderStyle(Slider::Rotary);
    frequencySlider.setTextBoxStyle(juce::Slider::TextEntryBoxPosition(0), false, 0, 0);
    frequencySlider.setValue(1000.0);
    
    qSlider.addListener(this);
    addAndMakeVisible(&qSlider);
    qSlider.setColour(Slider::rotarySliderFillColourId, Colours::darkblue);
    qSlider.setRange(0, 30.0);
    qSlider.setSliderStyle(Slider::Rotary);
    qSlider.setTextBoxStyle(juce::Slider::TextEntryBoxPosition(0), false, 0, 0);
    qSlider.setValue(7.0);
    
    gainSlider.addListener(this);
    addAndMakeVisible(&gainSlider);
    gainSlider.setColour(Slider::rotarySliderFillColourId, Colours::darkblue);
    gainSlider.setRange(0, 10.0);
    gainSlider.setSliderStyle(Slider::Rotary);
    gainSlider.setTextBoxStyle(juce::Slider::TextEntryBoxPosition(0), false, 0, 0);
    gainSlider.setValue(4.0);
    
    filterMode.addListener(this);
    addAndMakeVisible(&filterMode);
    filterMode.setColour(ComboBox::backgroundColourId, Colours::darkcyan);
    filterMode.setColour(ComboBox::arrowColourId, Colours::darkmagenta);
    filterMode.setColour(ComboBox::outlineColourId, Colours::darkcyan);
    filterMode.addItem("LP", 1);
    filterMode.addItem("HP", 2);
    filterMode.addItem("BP", 3);
    filterMode.setTextWhenNothingSelected("Choose Filter Mode");
    
    stateButton.addListener(this);
    addAndMakeVisible(&stateButton);
    stateButton.setToggleState(true, false);
    stateButton.setColour(TextButton::buttonColourId, Colours::darkblue);
    stateButton.setColour(TextButton::buttonOnColourId, Colours::beige);
    
    Font font("Courier", 12.0, 1);
    Justification justi(36);
    
    addAndMakeVisible(&frequencyLabel);
    frequencyLabel.setText("Centre Freq", false);
    frequencyLabel.setFont(font);
    frequencyLabel.setJustificationType(justi);
    
    addAndMakeVisible(&qLabel);
    qLabel.setText("Q", false);
    qLabel.setFont(font);
    qLabel.setJustificationType(justi);
    
    addAndMakeVisible(&gainLabel);
    gainLabel.setText("Gain", false);
    gainLabel.setFont(font);
    gainLabel.setJustificationType(justi);
    
}

FilterGui::~FilterGui()
{
    
}

//SliderCallbacks===============================================================
void FilterGui::sliderValueChanged (Slider* slider)
{
    if (!filterMode.getSelectedId() == 3 )
    {
        audioMidi.setMultiFilterParam(frequencySlider.getValue());
    }
    else
    {
        audioMidi.setMultiFilterParam(frequencySlider.getValue(), qSlider.getValue(), gainSlider.getValue());
    }
}

//ComboBoxCallbacks===============================================================
void FilterGui::comboBoxChanged (ComboBox* comboBoxThatHasChanged)
{
    if (filterMode.getSelectedId() == 1)
    {
        qSlider.setEnabled(false);
        gainSlider.setEnabled(false);
        audioMidi.setMultiFilterType(filterMode.getSelectedId());
        frequencySlider.setRange(20.0, 1000.0);
        
    }
    else if (filterMode.getSelectedId() == 2)
    {
        qSlider.setEnabled(false);
        gainSlider.setEnabled(false);
        audioMidi.setMultiFilterType(filterMode.getSelectedId());
        frequencySlider.setRange(400, 2000.0);
        
    }
    else if (filterMode.getSelectedId() == 3)
    {
        qSlider.setEnabled(true);
        gainSlider.setEnabled(true);
        audioMidi.setMultiFilterType(filterMode.getSelectedId());
        frequencySlider.setRange(50.0, 2000.0);
    }
}
//ButtonCallback===============================================================
void FilterGui::buttonClicked (Button* button)
{
    if (!stateButton.getToggleState())
    {
        frequencySlider.setEnabled(false);
        qSlider.setEnabled(false);
        gainSlider.setEnabled(false);
        filterMode.setEnabled(false);
    }
    else
    {
        frequencySlider.setEnabled(true);
        qSlider.setEnabled(true);
        gainSlider.setEnabled(true);
        filterMode.setEnabled(true);
    }
    
    audioMidi.setMultiFilterState(!audioMidi.getMultiFilterState());
    stateButton.setToggleState(audioMidi.getMultiFilterState(), false);
    
    
   
}

void FilterGui::resized()
{
    SInt16 x,y,width,height,sliderSpaceX, labelSpace,bsize;
 
    x = getWidth();
    y = getHeight();
    sliderSpaceX = 75;
    labelSpace = 45;
    width = getWidth()/5;
    height = getWidth()/5;
    bsize = getWidth()/20;
    
    
    
    filterMode.setBounds( 10, 10, width * 2, height/2 - 5);
    
    frequencySlider.setBounds( 10, 45, width, height);
    qSlider.setBounds(frequencySlider.getX() + sliderSpaceX, frequencySlider.getY(), width, height);
    gainSlider.setBounds(qSlider.getX() + sliderSpaceX, frequencySlider.getY(), width, height);
    
    frequencyLabel.setBounds(frequencySlider.getX(), frequencySlider.getY() + labelSpace, width, height/2);
    qLabel.setBounds(qSlider.getX(), qSlider.getY() + labelSpace,width, height/2);
    gainLabel.setBounds(gainSlider.getX(), gainSlider.getY() + labelSpace, width, height/2);
    
    stateButton.setBounds(gainSlider.getX() + 65, gainSlider.getY() + 50, bsize, bsize);
}

void FilterGui::paint (Graphics &g)
{
    g.setColour(Colours::darkblue);
    g.setOpacity(0.1);
    g.drawRoundedRectangle(0, 0, this->getWidth(), this->getHeight(), 8, 10);
    g.fillRoundedRectangle(0, 0, this->getWidth(), this->getHeight(), 8);
}