/*
 *  SimpleFilter.h
 */

#ifndef __SimpleFilter_h__
#define __SimpleFilter_h__

enum FilterType 
{ 
    kLPF = 0, 
    kHPF, 
    kBPF, 
    kBRF, 
    kNumberOfFilterTypes 
};

class SimpleFilter
{
public:

    
	SimpleFilter(float samplerate);
	void FilterConfig(FilterType type, float fc, float bw);
	float Filter(float sval);
private:
	float fSampleRate;
	unsigned short iType;
	float fFilta[3], fFiltb[3], fFiltvalx[3], fFiltvaly[3];
};

#endif
