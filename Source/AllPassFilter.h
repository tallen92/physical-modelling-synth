//
//  AllPassFilter.h
//  sdaAudioMidi
//
//  Created by Tyrone Allen on 27/02/2013.
//
//

#ifndef __sdaAudioMidi__AllPassFilter__
#define __sdaAudioMidi__AllPassFilter__

#include <iostream>
#include "../JuceLibraryCode/JuceHeader.h"
#include "TDelay.h"

/**
 Class for an all pass filter used in the waveguide model to fix tuning issues
 */

class AllPassFilter : public TDelay
{
public:
    
    /**
     Constructor
     */
    AllPassFilter();
    
    /**
     Deconstructor
     */
    ~AllPassFilter();
    
    /**
     All pass filter delays different frequencies by different amounts
     */
    Float32 filter(Float32 input);
    
private:
    
};

#endif /* defined(__sdaAudioMidi__AllPassFilter__) */
