//
//  SectionLabel.h
//  sdaAudioMidi
//
//  Created by Tyrone Allen on 11/03/2013.
//
//

#ifndef __sdaAudioMidi__SectionLabel__
#define __sdaAudioMidi__SectionLabel__

#include <iostream>
#include "../JuceLibraryCode/JuceHeader.h"

/**
 Base class for the Gui Labels
 */
class SectionLabel : public Component
{
    
public:
    
    SectionLabel();
    
    virtual ~SectionLabel();
    
    void resized();
    
	void paint (Graphics &g);
    
    /**
     Set the text of the label
     */
    void setLabelText(String newLabelText);
 
private:
    
       Label section;
};

#endif /* defined(__sdaAudioMidi__SectionLabel__) */
