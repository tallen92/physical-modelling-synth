//
//  DynamicLevelFilter.h
//  sdaAudioMidi
//
//  Created by Tyrone Allen on 27/02/2013.
//
//

#ifndef __sdaAudioMidi__DynamicLevelFilter__
#define __sdaAudioMidi__DynamicLevelFilter__

#include <iostream>
#include "../JuceLibraryCode/JuceHeader.h"
#include "TDelay.h"

/**
 Class for dynamic level low pass filter, derived from equations at https://ccrma.stanford.edu/realsimple/faust_strings/Dynamic_Level_Lowpass_Filter.html
 */
class DynamicLevelFilter : public TDelay
{
public:
    
    /**
     Constructor
     */
    DynamicLevelFilter();
    
    /**
     Deconstructor
     */
    ~DynamicLevelFilter();
    
    /*
     Filter input sample. Filter cutoff frequency is determined by the velocity of the midi note
     */
    Float32 filter(Float32 input);
    
private:
    
};

#endif /* defined(__sdaAudioMidi__DynamicLevelFilter__) */
