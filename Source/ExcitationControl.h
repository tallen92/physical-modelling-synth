//
//  ExcitationControl.h
//  sdaAudioMidi
//
//  Created by Tyrone Allen on 26/01/2013.
//
//

#ifndef __sdaAudioMidi__ExcitationControl__
#define __sdaAudioMidi__ExcitationControl__

#include <iostream>
#include "../JuceLibraryCode/JuceHeader.h"
#include "DynamicLevelFilter.h"
#include <cmath>

#define BUFFER_SIZE 1000

/**
 Modifies the nature of the excitation
 */

class ExcitationControl
{
public:
    
    /**
     Constructor
     */
	ExcitationControl();
    
    /**
     Destructor
     */
	~ExcitationControl();
    
    /**
     Configures a dynamic level low pass filters using the midi velocity value
     */
    void pickVelConfig(Float32 amp);
    
    /**
     Sets the pick position of the excitation which will be used as the delay length for pickPosition function
     
     Low value simulates near bridge twangy sound, middle value simulates fuller odd harmonic tone.
     */
    void setPickPos (Float32 pos);
    
    /**
     Simulates the creation of a standing wave due to the pick postion along the string through the use of a feedforward comb filter.
     */
    Float32 pickPosition (Float32 fexcite);
    
    /**
     Simulates the spectral centroid of the excitation being dependant of the amplitude of the excitation through the use of a dynamic level low pass filter.
     */
    Float32 pickVelocity (Float32 fIn);
    
    /**
     Excitation modifying process of the pick postion comb filter and the pick veloctiy filter
     */
    Float32 exciteProcess (Float32 fIn);
    
    
private:
    
    DynamicLevelFilter DLFilter;
    
    Atomic<Float32>pickPos;
    
    Float32 *pfCircularBuffer2;
    Float32 fSR2;
    SInt32 iBufferWritePos2;
    
};

#endif /* defined(__sdaAudioMidi__ExcitationControl__) */
